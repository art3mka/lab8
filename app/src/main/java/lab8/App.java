package lab8;

import java.util.Scanner;

import lab8.entity.Permutations;
import lab8.entity.Profit;
import lab8.entity.WaterSquare;


public class App {
    public static void main(String[] args) {
        try (Scanner numberScanner = new Scanner(System.in)) {
            System.out.print("Choose: 1 - PermutateArray, 2 - SharesProfit, 3 - WaterVolume:  ");
            int option = numberScanner.nextInt();
            if (option == 1) {
                Permutations.main(args);
                return;
            } else if (option == 2) {
                Profit.main(args);
                return;
            } else if (option == 3) {
                WaterSquare.main(args);
                return;
            } else {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            System.out.println("Wrong number");
        }
    }
}