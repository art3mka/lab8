package lab8.entity;

import java.util.ArrayList;
import java.util.Scanner;

public class Permutations {
    public static void main(String[] args) {
       try (Scanner input = new Scanner(System.in)) {
        System.out.println("Input: "); 
        String userString = input.nextLine();    
        int [] customList = stringConverter(userString);
        System.out.println(perms(customList));
        }
    }

    public static int[] stringConverter(String userString) {
        String[] stringArray = userString.split(",");

        int convertedArray[] = new int[stringArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            convertedArray[i] = Integer.parseInt(stringArray[i]);
        }

        return convertedArray;
    }

    public static ArrayList<ArrayList<Integer>> perms(int[] arr) {
		
		ArrayList<ArrayList<Integer>> output = new ArrayList<ArrayList<Integer>>();
		permsSupport(output, new ArrayList<>(), arr);
		return output;
	}

	public static void permsSupport(ArrayList<ArrayList<Integer>> output, ArrayList<Integer> list, int[] arr) {
		if (list.size() == arr.length) {
			output.add(new ArrayList<Integer>(list));
		} else {
			for (int i = 0; i < arr.length; i++) {
				if (list.contains(arr[i])) {
					continue;
				}
				list.add(arr[i]);
				permsSupport(output, list, arr);		
				list.remove(list.size() - 1);
			}
		}
	}
}
